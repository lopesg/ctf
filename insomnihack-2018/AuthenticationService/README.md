# Authentication Service (warmup / web)

Solves: 40 / Points: 85


As described in the challenge, it was just a warmup. You needed to find a vulnerability in an authentication form.

After some attempts trying to exploit classical SQL injections, it appears in fact it was a LDAP injection.

I tried several classical LDAP payloads, but at the end it appears a NULL byte was needed to close the request and then obtain the flag.

![Flag](./pics/flag.png "Flag")